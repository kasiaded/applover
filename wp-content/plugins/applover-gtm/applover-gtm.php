<?php
/**
 * Plugin Name:       Google Tag Manager (GTM)
 * Description:       This plugin adds possibility to add GTM code to your site
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Katarzyna Dędys
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       applover-gtm
**/

register_activation_hook( __FILE__, 'pluginprefix_function_to_run' );
function pluginprefix_function_to_run() {
	add_option('applover_gtm_code');
}

register_deactivation_hook( __FILE__, 'pluginprefix_deactivate' );
function pluginprefix_deactivate() {
	delete_option('applover_gtm_code');
}

add_action('wp_head', 'applover_gtm_insert_gtm');
function applover_gtm_insert_gtm () {
	?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','<?php echo get_option( 'applover_gtm_options' )['applover_gtm_code']; ?>');</script>
		<!-- End Google Tag Manager -->
	<?php
}

add_action( 'admin_menu', 'applover_gtm_add_settings_page' );
function applover_gtm_add_settings_page() {
    add_options_page( 'Google Tag Manager (GTM)', 'Google Tag Manager (GTM)', 'manage_options', 'applover-gtm', 'applover_render_plugin_settings_page' );
}

function applover_render_plugin_settings_page() {
    ?>
    <form action="options.php" method="post">
        <?php 
        settings_fields( 'applover_gtm_options' );
        do_settings_sections( 'applover_gtm' ); ?>
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    </form>
    <?php
}

add_action( 'admin_init', 'applover_gtm_register_settings' );
function applover_gtm_register_settings() {
    register_setting( 'applover_gtm_options', 'applover_gtm_options' );
    add_settings_section( 'applover_gtm_settings', 'Google Tag Manager Settings', 'applover_gtm__section_text', 'applover_gtm' );
    add_settings_field( 'applover_gtm_code', 'GTM', 'applover_gtm_code', 'applover_gtm', 'applover_gtm_settings' );
}

function applover_gtm__section_text() {
    echo '<p>Here you can set all the options for using the API</p>';
}

function applover_gtm_code() {
    $options = get_option( 'applover_gtm_options' );
    echo "<input id='applover_gtm_code' name='applover_gtm_options[applover_gtm_code]' type='text' value='" . esc_attr( $options['applover_gtm_code'] ) . "' />";
}