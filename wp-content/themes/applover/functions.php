<?php 
add_filter( 'use_block_editor_for_post', '__return_false' );
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}

function applover_enqueue_style() {
    wp_enqueue_style( 'applover', get_template_directory_uri() . '/dist/css/theme.css', false );
}


function applover_enqueue_script() {
	wp_enqueue_script('jquery');
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/dist/js/all.js', false, null, true );
}
 
add_action( 'wp_enqueue_scripts', 'applover_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'applover_enqueue_script' );

function zwp_add_menu() {
    register_nav_menu( 'main-menu', 'Menu główne' );
}

add_action('init','zwp_add_menu');

add_theme_support('post-thumbnails');
add_post_type_support( 'artykuly', 'thumbnail' ); 

function create_posttype() {
    register_post_type( 'artykuly',
        array(
            'labels' => array(
                'name' => __( 'Artykuły' ),
                'singular_name' => __( 'Artykuł' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'artykuly'),
            'menu_icon' => 'dashicons-format-aside',

        )
    );
}
add_action( 'init', 'create_posttype' );

add_theme_support( 'custom-logo' );

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );
   
/**
* Filter function used to remove the tinymce emoji plugin.
* 
* @param array $plugins 
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}
   
/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
	
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}
   
   return $urls;
}

// SVG and WebP Support

function codeless_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$new_filetypes['webp'] = 'image/webp';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_filter('upload_mimes', 'codeless_file_types_to_uploads');
define( 'ALLOW_UNFILTERED_UPLOADS', true );


// Customize Footer

function applover_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'footer_option' , array(
		'title'      => __( 'Footer', 'applover' ),
		'priority'   => 200,
	) );

	$wp_customize->add_setting( 'footer_logo' , array(
		'transport' => 'refresh',
		'type' => 'theme_mod',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'footer_logo',
			array(
				'label'      => 'Upload a logo to footer',
				'section'    => 'footer_option',
				'settings'   => 'footer_logo',
			)
		)
	);

	$wp_customize->add_setting( 'footer_info' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'footer_info_control',
		array(
			'label'    => __( 'Footer info', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'footer_info',
			'type'     => 'textarea',
		)
	);

	$wp_customize->add_setting( 'footer_facebook' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'footer_facebook_control',
		array(
			'label'    => __( 'Facebook link', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'footer_facebook',
			'type'     => 'url',
		)
	);

	$wp_customize->add_setting( 'footer_twitter' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'footer_twitter_control',
		array(
			'label'    => __( 'Twitter link', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'footer_twitter',
			'type'     => 'url',
		)
	);

	$wp_customize->add_setting( 'footer_pinterest' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'footer_pinterest_control',
		array(
			'label'    => __( 'Pinterest link', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'footer_pinterest',
			'type'     => 'url',
		)
	);

	$wp_customize->add_setting( 'footer_instagram' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'footer_instagram_control',
		array(
			'label'    => __( 'Instagram link', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'footer_instagram',
			'type'     => 'url',
		)
	);

	$wp_customize->add_setting( 'contact_text' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'contact_text_control',
		array(
			'label'    => __( 'Contact text', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'contact_text',
			'type'     => 'textarea',
		)
	);

	$wp_customize->add_setting( 'contact_email' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'contact_email_control',
		array(
			'label'    => __( 'Contact email', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'contact_email',
			'type'     => 'email',
		)
	);

	$wp_customize->add_setting( 'fixed_text' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'fixed_text_control',
		array(
			'label'    => __( 'Fixed text', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'fixed_text',
			'type'     => 'textarea',
		)
	);

	$wp_customize->add_setting( 'fixed_link' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		'fixed_link_control',
		array(
			'label'    => __( 'Fixed link', 'applover' ),
			'section'  => 'footer_option',
			'settings' => 'fixed_link',
			'type'     => 'url',
		)
	);
}

add_action( 'customize_register', 'applover_customize_register',5 );

// Customize Backgrounds

function applover_backgrounds_customize_register( $wp_customize ) {	
	$wp_customize->add_section( 'background_options' , array(
		'title'      => __( 'Backgrounds', 'applover' ),
		'priority'   => 500,
	) );

	$wp_customize->add_setting( 'background_hero' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'background_hero_control',
			array(
				'label'      => 'Upload hero background',
				'section'    => 'background_options',
				'settings'   => 'background_hero',
			)
		)
	);

	$wp_customize->add_setting( 'background_cta' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'background_cta_control',
			array(
				'label'      => 'Upload CTA background',
				'section'    => 'background_options',
				'settings'   => 'background_cta',
			)
		)
	);

	$wp_customize->add_setting( 'background_footer' , array(
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'background_footer_control',
			array(
				'label'      => 'Upload footer background',
				'section'    => 'background_options',
				'settings'   => 'background_footer',
			)
		)
	);
}

add_action( 'customize_register', 'applover_backgrounds_customize_register',6 );


// Customize Primary Color

function applover_color_customize_register( $wp_customize ) {

	$wp_customize->add_setting('primary_color', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_section('applover_standard_colors', array(
		'title' => __('Primary Color', 'applover'),
		'priority' => 600,
	));

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'primary_color_control', array(
		'label' => __('Change primary color', 'applover'),
		'section' => 'applover_standard_colors',
		'settings' => 'primary_color',
	) ) );

}

add_action('customize_register', 'applover_color_customize_register');

// Output Customize Primary color
function applover_color_customize_css() { ?>

	<style type="text/css">

		a:hover,
		a:visited,
		.btn,
		.footer__menu a:hover,
		.contact__link:hover {
			color: <?php echo get_theme_mod('primary_color'); ?>;
		}

		.gallery__ring::before {
			background-color: <?php echo get_theme_mod('primary_color'); ?>;
		}

		.social-media__image .a,
		.contact__icon .a {
			fill: <?php echo get_theme_mod('primary_color'); ?>;
		}

	</style>

<?php }

add_action('wp_head', 'applover_color_customize_css');
