(function ($, root, undefined) {
  const indicator = $(".navigation__desktop .menu-main-menu-container");
  const sections = $("[data-indicator]");

  let sectionPositions = [];
  let currentSection = 0;

  const indicatorFunction = () => {
    const indicatorPosition = indicator.offset().top + indicator.height() / 2;

    for (let i = 0; i < sectionPositions.length; i++) {
      if (indicatorPosition > sectionPositions[i]) {
        currentSection = i;
      }
    }

    if (sections[currentSection]) {
      const id = sections[currentSection].id;
      const links = $(".navigation__desktop a");
      const navDektop = $(".navigation__desktop");

      links.removeClass("active");
      $(`.navigation__desktop a[href="#${id}"]`).addClass("active");

      if ($(sections[currentSection]).attr("data-indicator") === "green") {
        links.addClass("alternative");
        navDektop.addClass("alternative");
      } else {
        links.removeClass("alternative");
        navDektop.removeClass("alternative");
      }
    }
  };

  const throttle = (fn, wait) => {
    let time = Date.now();
    return () => {
      if (time + wait - Date.now() < 0) {
        fn();
        time = Date.now();
      }
    };
  };

  const setSectionPositions = () => {
    sectionPositions = [];
    sections.each((index, element) => {
      sectionPositions.push($(element).offset().top);
    });
  };

  setSectionPositions();
  indicatorFunction();

  $(window).on("scroll", throttle(indicatorFunction, 50));
  $(window).on("resize", setSectionPositions);

  $(".navigation__hamburger").on("click", function (e) {
    $(".navigation__list").toggleClass("open-menu");
  });

  $(".navigation__list a").on("click", function (e) {
    e.preventDefault();
    $(".navigation__list").toggleClass("open-menu");
  });

  $('a[href*="#"]')
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        if (target.length) {
          event.preventDefault();
          $("html, body").animate(
            {
              scrollTop: target.offset().top,
            },
            1000,
            function () {
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {
                return false;
              } else {
                $target.attr("tabindex", "-1");
                $target.focus();
              }
            }
          );
        }
      }
    });
})(jQuery, this);
