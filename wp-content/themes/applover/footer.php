<!-- footer -->
<footer class="footer" role="contentinfo" data-indicator="green">
	<div class="footer__background-wrapper">
		<img class="footer__background" src="<?php echo get_theme_mod('background_footer');?>" alt="<?php _e("Footer Background");?>">
	</div>
	<div class="footer__container container">
		<div class="row">
			<div class="col-12 col-md-4 footer__info">
			<figure class="footer__logo">
				<?php echo file_get_contents(get_theme_mod('footer_logo'));?>
			</figure>
				<p class="footer__text"><?php echo get_theme_mod('footer_info'); ?></p>
				<div class="footer__social-media">
					<a class="social-media" target="_blank" href="<?php echo get_theme_mod('footer_facebook'); ?>">
						<figure class="social-media__image">
							<?php echo file_get_contents(get_template_directory_uri().'/img/facebook.svg'); ?>
						</figure>
					</a>
					<a class="social-media" target="_blank" href="<?php echo get_theme_mod('footer_twitter'); ?>">
						<figure class="social-media__image">
							<?php echo file_get_contents(get_template_directory_uri().'/img/twitter.svg'); ?>
						</figure>
					</a>
					<a class="social-media" target="_blank" href="<?php echo get_theme_mod('footer_pinterest'); ?>">
						<figure class="social-media__image">
							<?php echo file_get_contents(get_template_directory_uri().'/img/pinterest.svg'); ?>
						</figure>
					</a>
					<a class="social-media" target="_blank" href="<?php echo get_theme_mod('footer_instagram'); ?>">
						<figure class="social-media__image">
							<?php echo file_get_contents(get_template_directory_uri().'/img/instagram.svg'); ?>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-12 col-md-4 footer__menu">
				<h4 class="footer__menu-title">
				<?php _e("Menu");?>
				</h4>
				<?php wp_nav_menu( array( 'menu' => 'footer-menu' ) ); ?>
			</div>
			<div class="col-12 col-md-4 contact">
				<h4 class="contact__title"><?php _e("Kontakt");?></h4>
				<p class="contact__text"><?php echo get_theme_mod('contact_text'); ?></p>
				<div class="contact__email">
					<figure class="contact__icon">
						<?php echo file_get_contents(get_template_directory_uri().'/img/email.svg'); ?>
					</figure>
					<a href="mailto:<?php echo get_theme_mod('contact_email'); ?>" class="contact__link"><?php echo get_theme_mod('contact_email'); ?></a>
				</div>
			</div>
		</div>
	</div>
	<div class="fixed">
		<figure class="fixed__logo">
			<?php echo file_get_contents(get_theme_mod('footer_logo'));?>
		</figure>
		<h4 class="fixed__header"><?php echo get_theme_mod('fixed_text'); ?></h4>
		<p class="fixed__powered"><a class="fixed__powered-link" target="_blank" href="<?php echo get_theme_mod('fixed_link'); ?>"><?php _e("powered by applover.com");?></a></p>
	</div>
</footer>
<!-- /footer -->

<?php wp_footer(); ?>

</body>
</html>
