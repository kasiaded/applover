const gulp = require("gulp"),
  sass = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer"),
  rename = require("gulp-rename"),
  cssmin = require("gulp-cssmin"),
  concat = require("gulp-concat"),
  uglify = require("gulp-uglify"),
  imagemin = require("gulp-imagemin"),
  livereload = require("gulp-livereload"),
  jshint = require("gulp-jshint"),
  eyeglass = require("eyeglass");
const babel = require("gulp-babel");
const plumber = require("gulp-plumber");
const source = "assets/";
const dest = "dist/";

/**
 * Style :
 * Sass compilation including bootstrap-sass
 *
 */
gulp.task("sass", function () {
  return gulp
    .src(source + "/scss/style.scss")
    .pipe(plumber())
    .pipe(sass(eyeglass()))
    .pipe(autoprefixer())
    .pipe(cssmin())
    .pipe(rename("theme.css"))
    .pipe(gulp.dest(dest + "css"))
    .pipe(livereload());
});

/**
 * Js lint of main.js
 * stop gulp task on error
 *
 */
gulp.task("lint", function () {
  return gulp
    .src(source + "/js/main.js")
    .pipe(jshint())
    .pipe(jshint.reporter("jshint-stylish"));
});

/**
 * Javascript
 *
 */

gulp.task(
  "js",
  gulp.series("lint", function () {
    // Js librairies used on current theme
    gulp
      .src(source + "/js/**/*.js")
      .pipe(plumber())
      .pipe(
        babel({
          presets: [
            [
              "@babel/env",
              {
                modules: false,
              },
            ],
          ],
        })
      )
      .pipe(uglify())
      .pipe(concat("all.js"))
      .pipe(gulp.dest(dest + "js/"))
      .pipe(livereload());
  })
);

/**
 * Image compression
 *
 */
gulp.task("img", function () {
  return gulp
    .src(source + "/img/*.{png,jpg,gif}")
    .pipe(imagemin())
    .pipe(gulp.dest(dest + "images/"));
});

/**
 * Watch task
 *
 */
gulp.task("watch", function () {
  gulp.watch("assets/scss/**/*.scss").on("all", gulp.series("sass"));
  gulp.watch("assets/js/**/*.js").on("all", gulp.series("js"));
  // gulp.watch('assets/img/*.{png,jpg,gif}').on('all', gulp.series('img')); // to do: test
});

/**
 * Default
 * Should be launch at the beginning of every coding session
 *
 */
gulp.task(
  "default",
  gulp.series(gulp.parallel("sass", "js", "watch"), function () {
    return livereload.listen();
  })
);
