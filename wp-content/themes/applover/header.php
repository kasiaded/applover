<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<?php wp_head(); ?>

	</head>
	<body>
	<!-- header -->
	<header id="first-section" class="header clear" role="banner" data-indicator="white">
		<div class="container-fluid header__container">
			<div class="header__background-wrapper">
				<img class="header__background" src="<?php echo get_theme_mod('background_hero');?>" alt="<?php _e("Hero Background");?>">
			</div>
			<div class="header__logo-wrapper">
				<?php the_custom_logo(); ?>
			</div>
			<div class="header__navigation navigation">
				<!-- nav -->
				<nav class="nav navigation__desktop" role="navigation">
					<div class="navigation__wrapper">
						<?php wp_nav_menu( array( 'menu' => 'main-menu' ) ); ?>
					</div>
				</nav>
				<!-- /nav mobile -->	
				<nav class="nav navigation__list" role="navigation">
					
					<?php the_custom_logo(); ?>
					<?php wp_nav_menu( array( 'menu' => 'main-menu' ) ); ?>
			
				</nav>
				<div class="navigation__hamburger">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<?php 
			$heroFirstLine = get_post_meta($post->ID, 'Hero first line text', true);
			$heroSecondLine = get_post_meta($post->ID, 'Hero second line text', true);
			$heroThirdLine = get_post_meta($post->ID, 'Hero third line text', true);
			?>
			<div class="header__content">
				<h3 class="header__title"><?php echo $heroFirstLine;?></h3>
				<h2 class="header__title"><?php echo $heroSecondLine;?></h2>
				<h3 class="header__title"><?php echo $heroThirdLine;?></h3>
			</div>
			<a href="#second-section">
				<img class="header__scroll bounce" src="<?php echo get_template_directory_uri(); ?>/img/scroll.svg" alt="<?php _e('Scroll');?>">
			</a>
		</div>
	</header>
	<!-- /header -->
