<?php get_header(); ?>

<?php 
	$galleryTitle = get_post_meta($post->ID, 'Gallery title', true); 
	$ctaText = get_post_meta($post->ID, 'CTA text', true); 
	$buttonText = get_post_meta($post->ID, 'Button text', true); 

?>

<main class="main-width" role="main">
	<section id="second-section" class="container-fluid image-text" data-indicator="green">
		<div class="row">
		<?php 
			$args = array( 'post_type' => 'artykuly', 'posts_per_page' => 4 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="image-text__wrapper">
				<div class="col-12 col-lg-7 col-xl-5 image-text__image">
					<?php the_post_thumbnail(); ?>
				</div>
				<div class="col-12 col-lg-5 col-xl-7 image-text__text">
				<img class="image-text__ring" src="<?php echo get_template_directory_uri(); ?>/img/ring.svg" alt="">
					<h2 class="image-text__title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div>
	</section>

	<section id="third-section" class="gallery" data-indicator="green">
		<div class="gallery__container container">
			<h2 class="gallery__title">
				<?php echo $galleryTitle; ?>
			</h2>
			<div class="gallery__line">
				<div class="gallery__ring"></div>
			</div>
			<div class="gallery__box">
				<div class="gallery__wrapper">
				<?php 
				$gallery = get_post_meta($post->ID, 'gallery_image', false);
				if (!empty($gallery)) :
					foreach ( $gallery as $single ) :
				?>
					<div class="gallery__single">
						<img class="gallery__image" src="<?php echo $single;?>" alt="">
					</div>
				<?php
					endforeach;
				endif;
				?>
				</div>
			</div>
		</div>
	</section>

	<section class="cta container-fluid" data-indicator="white">
		<div class="cta__background-wrapper">
			<img class="cta__background" src="<?php echo get_theme_mod('background_cta');?>" alt="<?php _e("CTA Background");?>">	
		</div>
		<?php the_custom_logo(); ?>
		<p class="cta__text"><?php echo $ctaText; ?></p>
		<button class="btn"><?php echo $buttonText; ?></button>
	</section>
</main>
<?php get_footer(); ?>
